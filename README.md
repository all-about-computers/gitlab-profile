# Contents

- [2024년 백엔드 및 웹 개발 트렌드](https://all-about-computers.gitlab.io/back-end-and-web-development-trends-for-2024) 2023-12-31
- [2024년에 고려해야 할 상위 10 플랫폼 엔지니어링 도구](https://all-about-computers.gitlab.io/top-10-platform-engineering-tools-in-2024) 2024-01-14
- [2024년 상위 15 소프트웨어 개발 트렌드](https://all-about-computers.gitlab.io/top-15-software-development-trends-in-2024) 2024-01-15
- [2024년 GitHub vs GitLab](https://all-about-computers.gitlab.io/github-vs-gitlab-in-2024/)<sup><img src="./images.png" width="15" height="15" /></sup> 2024-03-02

## To be read
- [2024 Data Engineering Trends](https://levelup.gitconnected.com/2024-data-engineering-trends-c4e1e03e0634) 2024-02-08
- [2025년 쿠버네티스 표준 아키텍처](https://yozm.wishket.com/magazine/detail/2900/?data=F0UHbVER8khMP%2ffAAQUll1T1ecSFcZsai1%2fWijMWy%2f0=&source=daily_latest_news) 2024-12-24
